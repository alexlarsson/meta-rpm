#!/usr/bin/env python3

import sys, os, json, rpm, yaml, re, argparse, logging, dnf, hawkey

def mkdir_p(path):
    try:
        os.mkdir(path)
    except FileExistsError:
        pass

class LicenseMap:
    def __init__(self, json_path):
        self.ldict = {}
        with open(json_path, "r") as lconfig_f:
            self.ldict = json.load(lconfig_f)
        self.regex = re.compile("|".join(map(re.escape, self.ldict.keys(  ))))

    def map_from_rpm(self, ltext):
        return self.regex.sub(lambda match: self.ldict[match.group(0)], ltext)

def split_rpm_filename(filename):
    if filename[-4:] == '.rpm':
        filename = filename[:-4]
    arch_index = filename.rfind('.')
    arch = filename[arch_index+1:]
    rel_index = filename[:arch_index].rfind('-')
    rel = filename[rel_index+1:arch_index]
    ver_index = filename[:rel_index].rfind('-')
    ver = filename[ver_index+1:rel_index]
    epochIndex = filename.find(':')
    if epochIndex == -1:
        epoch = ''
    else:
        epoch = filename[:epochIndex]
    name = filename[epochIndex + 1:ver_index]
    return name, ver, rel, epoch, arch

class SrpmMapping:
    def __init__(self, srpm_pkg_name, data, mapping):
        self.srpm_pkg_name = srpm_pkg_name
        self.mapping = mapping
        self.recipe = None
        self.nonative = False
        self.subrecipes = {}
        self.rprovides = {}
        self.patch = None
        self.disabled = False
        self.rpms = []
        self.depends = []
        self.nodepends = []
        self.provides = []

        if "subrecipes" in data:
            subrecipes = data["subrecipes"]
            del data["subrecipes"]
            for subname, subdata in subrecipes.items():
                if subdata == None:
                    subdata = {}
                sub = SrpmMapping(srpm_pkg_name, subdata, mapping)
                # If no rpms are specified, assume it meant the one identical to the recipe name
                if len(sub.rpms) == 0:
                    sub.rpms.append(subname)
                self.subrecipes[subname] = sub

        for key in ["recipe", "patch", "disabled", "rpms", "depends", "nodepends", "rprovides", "provides", "nonative"]:
            if key in data:
                setattr(self, key, data[key])
                del data[key]

        for key in data.keys():
            print("WARNING: Unexpected srpm mapping key %s for srpm %s" % (key, srpm_pkg_name))


class Mapping:
    def __init__(self, data, license_map):
        self.preferred_packages = {}
        self.enabled_streams = []
        self.ignore_rpms = []
        self.srpms = {}
        self.license_map = license_map
        self.arches = []

        if "srpms" in data:
            srpms = data["srpms"]
            del data["srpms"]
            for srpm_pkgname, srpm_data in srpms.items():
                srpm_mapping = SrpmMapping(srpm_pkgname, srpm_data, self)
                if srpm_pkgname in self.srpms:
                    print("WARNING: Duplicate srpms %s in mapping" % (srpm_pkgname))
                self.srpms[srpm_pkgname] = srpm_mapping

        for key in ["preferred-packages", "enabled-streams", "ignore-rpms", "arches"]:
            if key in data:
                setattr(self, key.replace("-", "_"), data[key])
                del data[key]

        for key in data.keys():
            print("WARNING: Unexpected global mapping key %s" % (key))

    def get_srpm_mapping(self, srpm_pkg_name):
        return mapping.srpms.get(srpm_pkg_name, SrpmMapping(srpm_pkg_name, {}, self))

    def get_recipe_name_for_srpm_pkg_name (self, srpm_pkg_name):
        srpm_mapping = self.get_srpm_mapping(srpm_pkg_name)
        if srpm_mapping.recipe:
            return srpm_mapping.recipe
        # default: Convert _ to -, and lowercase (yocto warns otherwise)
        return srpm_pkg_name.replace("_", "-").lower()

class RpmData:
    def __init__(self, rpm_name, data):
        self.rpm_name = rpm_name
        (pkg_name, ver, rel, epoch, arch) = split_rpm_filename(rpm_name)
        self.pkg_name = pkg_name
        self.evr = (epoch, ver, rel)
        self.arch = arch

        self.file_provides = []
        self.file_requires = []
        self.license = None
        self.modularitylabel = None
        self.provides = []
        self.requires = []
        self.srpm = None
        self.sha256: None
        self.url = None

        for key, val in data.items():
            setattr(self, key.replace("-", "_"), val)

        assert self.rpm_name == self.rpm # TODO: Stop generating self.rpm in json

        self.stream_id = None
        if self.modularitylabel:
            parts = self.modularitylabel.split(":")
            if len (parts) < 2:
                print ("WARNING: Unexpected modularity label format: %s" % (self.modularitylabel))
            else:
                self.stream_id = parts[0] + ":" + parts[1]

class RpmsData:
    def __init__(self, dnf_data, mapping):
        self.printed_prefers = {}
        self.non_modular_rpms = {}
        self.streams = {}

        for rpm_name, dnf_rpm_data in dnf_data.items():
            rpm_data = RpmData(rpm_name, dnf_rpm_data)
            pkg_name = rpm_data.pkg_name

            if pkg_name in mapping.ignore_rpms:
                continue

            if rpm_data.stream_id:
                stream_id = rpm_data.stream_id
                if not stream_id in self.streams:
                    self.streams[stream_id] = {}

                dest = self.streams[stream_id]
            else:
                dest = self.non_modular_rpms

            # Only enable the pkg_name with highest evr (for each stream)
            if pkg_name in dest:
                old_rpm_data = dest[pkg_name]
                if rpm.labelCompare(rpm_data.evr, old_rpm_data.evr) < 0:
                    continue # This is older than what we've seen, ignore
            dest[pkg_name] = rpm_data

        self.compute_enabled_rpms(mapping)
        self.compute_by_srpm()

        self.compute_providers()
        self.compute_rdepends(mapping)

    def compute_by_srpm(self):
        self.by_srpm = {}
        for pkg_name, rpm_data in self.rpms.items():
            srpm = rpm_data.srpm
            if not srpm in self.by_srpm:
                self.by_srpm[srpm] = []
            self.by_srpm[srpm].append(rpm_data)

    # Filter out the set of binary rpms that are not ignored, and
    # is enabled by the enabled module sets
    def compute_enabled_rpms(self, mapping):
        self.rpms = {}

        modular_rpms = {}
        # TODO: This just adds all the matching rpms for the stream, irrespective
        # of the NSVCA module version/context, and we let the NEVR sort handle duplicates
        # later. This isn't strictly correct, so this needs further work.
        for enabled in mapping.enabled_streams:
            if not enabled in self.streams:
                print ("WARNING: Specified enable stream '%s' does not exist" % (enabled))
                continue
            stream = self.streams[enabled]
            for pkg_name, rpm_data in stream.items():
                self.rpms[pkg_name] = rpm_data
                # If we used a pkg-name from a module, don't inherit it from base
                modular_rpms[pkg_name] = True

        for pkg_name, rpm_data in self.non_modular_rpms.items():
            if pkg_name in modular_rpms:
                continue
            self.rpms[pkg_name] = rpm_data

    # Generate reverse mapping from provide string to list of providers (by pkgname)
    def compute_providers(self):
        self.providers = {}
        for pkg_name, rpm_data in self.rpms.items():
            for prov in rpm_data.provides:
                prov = prov.split(" ")[0] # We ignore any versioning
                if not prov in self.providers:
                    self.providers[prov] = []
                if not pkg_name in self.providers[prov]:
                    self.providers[prov].append(pkg_name)
            for prov in rpm_data.file_provides:
                prov = prov.split("\t")[0] # We ignore these weird tab things
                if not prov in self.providers:
                    self.providers[prov] = []
                if not pkg_name in self.providers[prov]:
                    self.providers[prov].append(pkg_name)

    def find_provider_for_req(self, pkg_name, req, mapping):
        if req.startswith("rpmlib("): # These are handled internally
            return None
        if req.startswith("font("): # Font are best handled manually
            return None
        if req.startswith("("): # No support for optional requires
            return None
        req = req.split(" ")[0] # We ignore any versioning
        if not req in self.providers:
            print("WARNING: package %s requires %s, but it has no provider" % (pkg_name, req))
            return None
        providers = self.providers[req]
        if pkg_name in providers:
            return None # Some packages provider their own requires, in that case we don't add any deps

        if len(providers) == 1: # If there is only one, use that
            return providers[0]
        elif req in mapping.preferred_packages:
            return mapping.preferred_packages[req]
        elif req in providers: # Sometime things require a package name, but optionally other things supply it, go with the real thing
            return req

        # As fallback we pick the shortest named provider
        # This is stable over order-changes and prefers "foo" over "foo-special"
        provider = min(sorted(providers), key=len)

        providers_as_string = str(providers)
        printed_warning_already = providers_as_string in self.printed_prefers
        self.printed_prefers[providers_as_string] = True
        if not printed_warning_already:
            print("NOTE: package %s requires %s, but it has several providers: %s, picking shortest (%s)" % (pkg_name, req, providers, provider))

        return provider

    # Resolves rpm requires strings to real rpm pkg names
    def compute_rdepends(self, mapping):
        for pkg_name, rpm_data in self.rpms.items():
            rdepends = set()
            for req in rpm_data.requires:
                provider = self.find_provider_for_req(pkg_name, req, mapping)
                if provider:
                    rdepends.add(provider)
            rpm_data.rdepends = sorted(list(rdepends))

def extract_soname_dep(rpm_prov):
    if rpm_prov.startswith("/"): # It's a file provides (like "/bin/bash")
        return None
    parts = rpm_prov.split (" ")
    first = parts[0].split ("(")[0]
    if ".so" in first:
        return first
    return None

class RecipeBook:
    def __init__(self, arch):
        self.arch = arch
        self.recipes = {}
        self.recipes_by_rpm = {}

    def add(self, recipe):
        recipe_name = recipe.recipe_name
        if recipe_name in self.recipes:
            print("Warning: recipe %s already exists" % (recipe_name))
            assert recipe_name not in book.recipes
        self.recipes[recipe_name] = recipe

class Recipe:
    class Package:
        def __init__(self, book):
            self.prov_sonames = set()
            self.req_sonames = set()
            self.rprovides = []
            self.rdepends = []
            self.book = book

    def __init__(self, book, recipe_name, srpm_pkg_name, mapping):
        self.book = book
        self.recipe_name = recipe_name
        self.srpm_pkg_name = srpm_pkg_name
        self.mapping = mapping
        self.srpm_full_names = []
        self.rpms_data = {} # pkg name to data
        self.subrecipes = {}
        self.depends = self.mapping.depends.copy()
        self.provides = self.mapping.provides.copy()

        self.patches = []
        if self.mapping.patch:
            if type(self.mapping.patch) is list:
                self.patches = self.mapping.patch
            else:
                self.patches = [ self.mapping.patch ]

        book.add(self)

        for subrecipe, subrecipe_data in mapping.subrecipes.items():
            if subrecipe in book.recipes:
                print("Warning: recipe %s split out from srpm %s already exists" % (subrecipe, srpm_pkg_name))
            self.subrecipes[subrecipe] = Recipe(book, subrecipe, srpm_pkg_name, subrecipe_data)

    def add_rpms(self, srpm_full_name, rpms_data):
        self.srpm_full_names.append(srpm_full_name)
        for rpm_data in rpms_data:
            target_recipe = self
            pkg_name = rpm_data.pkg_name
            for subrecipe_name, subrecipe in self.subrecipes.items():
                if pkg_name in subrecipe.mapping.rpms:
                    target_recipe = self.subrecipes[subrecipe_name]
                    break
            target_recipe.rpms_data[pkg_name] = rpm_data
            if pkg_name in self.book.recipes_by_rpm:
                print("Warning: multiple recipes for package name %s" % (pkg_name))
            self.book.recipes_by_rpm[pkg_name] = target_recipe

    def add_automatic_dependencies(self, rpms_data, mapping):
        # We try to auto-generate dependencies for devel packages.
        # For instance if libfoo-devel has a header that includes a header for libbar-devel
        # then libfoo needs to DEPENDS on libbar to get the header into the sysroot.
        # We don't really have this info, but we try to reconstruct it based on the
        # pkgconfig dependencies and manual dependencies from *-devel on another *-devel
        # To avoid some tricky loops we avoid handling dependencies for some core stuff
        if self.recipe_name in ["glibc", "binutils", "selinux-policy"]:
            return
        has_pcfile = False
        for pkg_name, rpm_data in self.rpms_data.items():
            dep_providers = set()
            for req in rpm_data.requires:
                req_prefix = req.split(" ")[0].split("(")[0] # Strips version checks and ()
                if req.startswith("pkgconfig(") or (pkg_name.endswith("-devel") and req_prefix.endswith("-devel")):
                    provider = rpms_data.find_provider_for_req(pkg_name, req, mapping)
                    if provider:
                        dep_providers.add(provider)
                else:
                    soname = extract_soname_dep(req)
                    if soname:
                        provider = rpms_data.find_provider_for_req(pkg_name, req, mapping)
                        if provider:
                            # We don't create automatic dependencies between same srpms
                            # If its the same recipe (which is the default) that makes no sense,
                            # but if the srpm is split that adding dependencies between the two
                            # is prone to cycles, so we force the user to manually specify those deps
                            provider_data = rpms_data.rpms[provider]
                            if provider_data.srpm != rpm_data.srpm:
                                dep_providers.add(provider)

            for provider in dep_providers:
                dep_rec = self.book.recipes_by_rpm[provider]
                if dep_rec and dep_rec.recipe_name != self.recipe_name and \
                   dep_rec.recipe_name not in ["kernel", "glibc", "gcc-runtime"] and \
                   dep_rec.recipe_name not in self.mapping.nodepends and \
                   dep_rec.recipe_name not in self.depends:
                    self.depends.append(dep_rec.recipe_name)

            for prov in rpm_data.provides:
                prov_prefix = prov.split(" ")[0].split("(")[0] # Strip version checks and ()
                if prov.startswith("pkgconfig("):
                    has_pcfile = True

        # Add pkg-config native dependency if the package has a pc-file so it can be used
        # We skip this for pkgconf and pkgconfig to avoid circular deps
        if has_pcfile and "pkgconfig-native" not in self.depends and not self.recipe_name.startswith("pkgconf"):
            self.depends.append("pkgconfig-native")

    def compute(self):
        # This is called when all the rpm datas have been added to the recipe

        self.rpm_arch = "allarch"
        self.rpm_license = ""
        self.license = ""
        self.packages = {}
        self.urls = []
        self.sha256s = {}

        for pkg_name in sorted(self.rpms_data.keys()):
            rpm_data = self.rpms_data[pkg_name]
            package = Recipe.Package(self.book)
            self.packages[pkg_name] = package

            if rpm_data.arch != "noarch":
                self.rpm_arch = rpm_data.arch

            self.urls.append(rpm_data.url)
            self.sha256s[rpm_data.url] = rpm_data.sha256

            self.rpm_license = rpm_data.license
            self.license = self.mapping.mapping.license_map.map_from_rpm (self.rpm_license)

            package.version = rpm_data.evr[1]

            package.rprovides = self.mapping.rprovides.get(pkg_name, []).copy()

            locales = []
            for file in rpm_data.file_provides:
                if file.startswith ("/usr/lib/locale/") and file.endswith("LC_CTYPE"):
                    locale = os.path.basename(os.path.dirname(file))
                    if locale != "C" and not locale.startswith("C.") and locale.find("_") != -1:
                        l = tuple(locale.lower().split("_", 1))
                        locales.append(l)

            gconvs = []
            for file in rpm_data.file_provides:
                if (file.startswith ("/usr/lib/gconv/") or file.startswith ("/usr/lib64/gconv/")) and file.endswith(".so"):
                    gconv = os.path.basename(file)[:-3].lower()
                    gconvs.append(gconv)

            if pkg_name.endswith("-devel"):
                package.rprovides.append(pkg_name.replace("-devel", "-dev"))

            for locale in locales:
                package.rprovides.append("locale-base-%s-%s" % locale)
                package.rprovides.append("virtual-locale-%s-%s" % locale)
                package.rprovides.append("virtual-locale-%s" % (locale[0]))

            for gconv in gconvs:
                package.rprovides.append("glibc-gconv-%s" % gconv)

            for prov in rpm_data.provides:
                soname = extract_soname_dep(prov)
                if soname:
                    package.prov_sonames.add(soname)

            for req in rpm_data.requires:
                soname = extract_soname_dep(req)
                if soname:
                    package.req_sonames.add(soname)

            if len(rpm_data.rdepends) > 0:
                package.rdepends = rpm_data.rdepends.copy()

            for pn in self.mapping.rprovides:
                if pn not in self.rpms_data:
                    print("WARNING: %s specifies an rprovides for non-existing rpm %s" % (self.srpm_pkg_name, pn))


def compute_active_recipes(rpms_data, mapping, arch):
    book = RecipeBook(arch)

    # Compute recipes for each srpm, renaming as necessary
    # Note that there may be multiple versions of the same srpm, either
    # because it was updated, or it was build with different options producing
    # different rpm names (e.g in modules)
    for srpm_name, srpm_rpms_data in rpms_data.by_srpm.items():
        (srpm_pkg_name, ver, rel, epoch, arch) = split_rpm_filename(srpm_name)
        recipe_name = mapping.get_recipe_name_for_srpm_pkg_name(srpm_pkg_name)

        if recipe_name in book.recipes:
            # We have multiple srpms with the same pkg name (different versions), reuse Recipe
            recipe = book.recipes[recipe_name]
        else:
            srpm_mapping = mapping.get_srpm_mapping(srpm_pkg_name)
            recipe = Recipe(book, recipe_name, srpm_pkg_name, srpm_mapping)

        recipe.add_rpms(srpm_name, srpm_rpms_data)

    for recipe_name, recipe in book.recipes.items():
        recipe.compute()
        recipe.add_automatic_dependencies(rpms_data, mapping)

    return book

logging.basicConfig(format='%(levelname)s: %(message)s')
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)

def get_dnf_data(dnf_dir, arch):
    dnf_dir = os.path.abspath(os.path.join(dnf_dir))
    base = dnf.Base()
    base.conf.cachedir = dnf_dir + "/cache"
    base.conf.config_file_path = dnf_dir + "/dnf.conf"
    base.conf.reposdir = dnf_dir + "/repos"
    base.conf.substitutions['arch'] = arch
    base.conf.substitutions['basearch'] = dnf.rpm.basearch(arch)
    base.conf.substitutions.update_from_etc(base.conf.installroot, varsdir=(dnf_dir + "/vars/", dnf_dir + "/vars/"))

    base.read_all_repos()

    # read the metadata
    base.fill_sack()

    query = base.sack.query().filter(reponame__neq="@System")
    packages = {}

    # We parse modular metadata separately from normal metadata, and if we want
    # to assign the modularitylabel back to the packages (artifacts) we have to
    # be able to search in the "packages" dictionary quickly using the artifacts
    # format (nevra).
    map_by_artifact_name = {}

    for package in query:
        basename = os.path.basename(package.location)
        if basename in packages:
            LOG.error("package %s is duplicated", basename)
            continue

        if package.arch != arch and package.arch != "noarch":
            continue # Skip non-primary multi-arch

        pkgdata = {}
        pkgdata["rpm"] = basename
        pkgdata["license"] = package.license
        assert hawkey.chksum_name(package.chksum[0]) == "sha256"
        pkgdata["sha256"] = package.chksum[1].hex()
        pkgdata["srpm"] = package.sourcerpm
        pkgdata["requires"] = sorted([str(r) for r in package.requires])
        pkgdata["provides"] = sorted([str(r) for r in package.provides])
        pkgdata["file-provides"] = sorted(package.files)
        pkgdata["url"] = package.remote_location()

        packages[basename] = pkgdata

        lookup_string = "{}-{}:{}-{}.{}".format(
            package.name, package.epoch, package.version, package.release,
            package.arch,
        )

        assert lookup_string not in map_by_artifact_name
        map_by_artifact_name[lookup_string] = pkgdata

    mbase = dnf.module.module_base.ModuleBase(base)
    for module in mbase.get_modules('*')[0]:
        artifacts = module.getArtifacts()
        module = module.getFullIdentifier()
        for artifact in artifacts:
            if artifact in map_by_artifact_name:
                package = map_by_artifact_name[artifact]
                assert "modularitylabel" not in package
                package["modularitylabel"] = module

    # check that all seemingly modular packages have modularitylabel
    for package in packages:
        if ".module_" in package:
            assert "modularitylabel" in packages[package]

    return packages


def list_all_same(vs):
    vs = list(vs)
    if len(vs) == 0:
        return True
    v = vs[0]
    for v2 in vs[1:]:
        if v != v2:
            return False
    return True

# Get a recipe key, asserts all recipes have same value
def get_same (recipes, key):
    assert (len(recipes) > 0)

    first = True
    v = None
    for r in recipes:
        if first:
            first = False
            v = getattr(r, key)
        else:
            v2 = getattr(r, key)
            if v2 != v:
                print("Error: in recipe %s, key %s is different between two arches (values: '%s' and '%s')" % (r.recipe_name, key, v, v2))
                exit(1)
    return v

class Formatter:
    def __init__(self, all_arches):
        self.lines = []
        self.assigned = {}
        self.all_arches = all_arches

    def add_line(self, line):
        self.lines.append(line)

    def make_arched_variable (self, variable, arch):
        if arch:
            return '%s_%s' % (variable, arch.replace("_", "-"))
        return variable

    def add_assign(self, variable, arch, value):
        full_variable = self.make_arched_variable (variable, arch)
        if full_variable in self.assigned:
            if self.assigned[full_variable] != value:
                print("ERROR: Trying to emit different values for variable '%s' (values '%s', '%s')" % (full_variable, value, self.assigned[full_variable]))
                exit(1)
            return
        self.assigned[full_variable] = value
        self.add_line('%s = "%s"' % (full_variable, value))

    def add_var(self, variable, recipes, fn):
        vs = {}
        v = None
        for r in recipes:
            arch = r.book.arch
            v = fn(r)
            vs[arch] = v

        if list_all_same(vs.values()):
            self.add_assign(variable, None, v)
        else:
            for arch, v2 in vs.items():
                self.add_assign(variable, arch, v2)

    def add_list_var(self, variable, recipes, fn):
        vs = {}
        v = None

        for r in recipes:
            arch = r.book.arch
            v = set(fn(r))
            vs[arch] = v

        # Compute shared elements for all arches
        shared = None
        for arch in self.all_arches:
            v = vs.get(arch, set())

            if shared == None:
                shared = v.copy()
            else:
                shared.intersection_update(v)

        if shared:
            self.add_assign(variable, None, " ".join(sorted(shared)))

        for arch, v in vs.items():
            remaining = v.difference(shared)
            if remaining:
                self.add_assign(variable + "_append", arch, " " + " ".join(sorted(remaining)))

    def to_string(self):
        return '\n'.join(self.lines) + '\n'

# We generate the recipe for each architecture, and then the format code combines these into one
# single file as needed
def format_recipe(recipes, all_arches):
    first_recipe = recipes[0]
    # The mapping will always be the same for all arches, because we only have one mapping file
    # So, we can reference it immediately
    mapping = first_recipe.mapping

    f = Formatter(all_arches)

    f.add_var("SUMMARY", recipes, lambda r: "generated recipe based on %s srpm" % (r.srpm_pkg_name))
    f.add_assign("DESCRIPTION", None, "Description")
    f.add_var("LICENSE", recipes, lambda r: r.license)
    f.add_var("RPM_LICENSE", recipes, lambda r: r.rpm_license)
    f.add_assign("LIC_FILES_CHKSUM", None, "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca")
    f.add_var("PACKAGE_ARCH", recipes, lambda r: r.rpm_arch)
    f.add_line("inherit rpmbased")

    if not mapping.nonative:
        f.add_assign("BBCLASSEXTEND", None, "native")

    f.add_list_var("PROVIDES", recipes, lambda r: r.provides)
    f.add_list_var("DEPENDS", recipes, lambda r: r.depends)

    # Make a dict of pkg-name => list of Recipe.Package from each arch
    # These can be used as Recipes in the formater, because pkg.book.arch works like recipe.book.arch
    packages = {}
    for r in recipes:
        for pkg_name, package in r.packages.items():
            if not pkg_name in packages:
                packages[pkg_name] = []
            packages[pkg_name].append(package)

    for pkg_name, arch_pkgs in packages.items():
        f.add_list_var('RPM_SONAME_PROV_%s' % (pkg_name), arch_pkgs, lambda pkg: pkg.prov_sonames)
        f.add_list_var('RPM_SONAME_REQ_%s' % (pkg_name), arch_pkgs, lambda pkg: pkg.req_sonames)
        f.add_list_var('RPROVIDES_%s' % (pkg_name), arch_pkgs, lambda pkg: map(lambda prov: "%s (= %s)" % (prov, pkg.version), pkg.rprovides))
        f.add_list_var('RDEPENDS_%s' % (pkg_name), arch_pkgs, lambda pkg: pkg.rdepends)

    f.add_var("RPM_URI", recipes, lambda r: ' \\\n                   '.join (map(lambda url: url + " " + r.sha256s[url], sorted(r.urls))))

    f.add_list_var("SRC_URI", recipes, lambda r: map(lambda p: 'file://%s' % (p), r.patches))

    return f.to_string()


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--dnfdir", dest='dnfdir', required=True)
    parser.add_argument("--mapping", dest='mapping', required=True)
    parser.add_argument("--destdir", dest='destdir', required=True)
    parser.add_argument("--licenses", dest='licenses', required=True)

    args = parser.parse_args()

    mapping_path = args.mapping
    dest_dir = args.destdir

    license_map = LicenseMap(args.licenses)

    with open(mapping_path) as yaml_file:
        mapping_data = yaml.safe_load(yaml_file)

    mapping = Mapping(mapping_data, license_map)

    # Generate recipes for each arch

    books = {}
    for arch in mapping.arches:
        packages = get_dnf_data(args.dnfdir, arch)
        rpms_data = RpmsData(packages, mapping)
        books[arch] = compute_active_recipes(rpms_data, mapping, arch)

    # List all recipe names that are in some arch
    recipe_names = set()
    for arch, arch_book in books.items():
        for recipe_name in arch_book.recipes.keys():
            recipe_names.add(recipe_name)

    # Write the recipes, combining from all arches
    mkdir_p(dest_dir)

    for recipe_name in recipe_names:
        recipes = []
        for arch, arch_book in books.items():
            if recipe_name in arch_book.recipes:
                recipe = arch_book.recipes[recipe_name]
                if not recipe.mapping.disabled:
                    recipes.append(recipe)

        if len(recipes) == 0:
            continue

        content = format_recipe(recipes, mapping.arches)
        path = os.path.join(dest_dir, recipe_name + ".bb")
        with open(path, 'w') as outfile:
            outfile.write(content)
