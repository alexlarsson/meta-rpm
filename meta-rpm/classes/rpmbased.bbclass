# Requirement:
#  The yocto build must be using BASELIB=lib64

# ISSUES:
#  * The recipes must list DEPENDS on the recipes of the
#    dependencies, or the shlib resolving wont work because
#    the dependent recipes pkgdata is not in the pkgdata-sysroot
#    so we can't resolve sonames to pkgnames

# Skip the unwanted build steps
do_configure[noexec] = "1"
do_compile[noexec] = "1"
do_patch[noexec] = "1"
do_patch[depends] = ""

NON_RPMBASED_DEPENDS = ""

INSANE_SKIP_${PN} = "already-stripped"
INHIBIT_DEFAULT_DEPS = "1"
ALLOW_EMPTY_${PN} = "1"

# Since all extracted rpms are usr-merged we override SYSROOT_DIRS to
# avoid staging both e.g. /usr/bin and /bin, as the later is just
# a symlink to the first (which duplicates it).
# Note: We really need to stage .../usr/lib not .../lib because
# otherwise relative symlinks inside the usr/ directory don't work.
SYSROOT_DIRS = " \
    ${includedir} \
    ${libdir} \
    ${nonarch_libdir} \
    ${datadir} \
"
SYSROOT_DIRS_NATIVE = " \
    ${bindir} \
    ${sbindir} \
    ${libexecdir} \
    ${sysconfdir} \
    ${localstatedir} \
"
# On cross builds, ${libdir} is in /usr/lib/x86_64-redhat-linux/, but we also need
# to dist the regular /usr/lib64, as it has e.g. libbfd.so for binutils.
SYSROOT_DIRS_NATIVE_append_class-cross = "${base_libdir}"

# The default just has /lib, but due to usrmerge these will end up in usr/lib, so relocate that too
PREPROCESS_RELOCATE_DIRS += "${nonarch_libdir}"

# Conpute the -cross-xxx part of the name
# If the build and target arch is the same we use the standard -cross-$target name as the prebuild rpms work fine
# But if they differ we can't use them, so we name it -disabled-cross-$target so that we can pick up a different cross-$target
def cross_suffix(d):
    build_arch = d.getVar("BUILD_ARCH")
    target_arch = d.getVar("TARGET_ARCH")
    if build_arch != target_arch:
      return "-disabled-cross-%s" % target_arch
    else:
      return "-cross-%s" % target_arch

# This is a version of the function in chrpath that uses patchelf (instead of chrpath) so we can add rpaths when one doesn't exist
def process_file_linux(cmd, fpath, rootdir, baseprefix, tmpdir, d, break_hardlinks = False):
    import subprocess, oe.qa

    with oe.qa.ELFFile(fpath) as elf:
        try:
            elf.open()
        except oe.qa.NotELFFileError:
            return

    try:
        out = subprocess.check_output(["patchelf", "--print-rpath", fpath], universal_newlines=True)
    except subprocess.CalledProcessError as e:
        return

    curr_rpath = out.rstrip()
    rpaths = []
    if curr_rpath:
        rpaths = curr_rpath.strip().split(":")
    new_rpaths = []

    # TODO: Should read out BASE_LIB or something instead of hardcoding lib64
    rpaths.append(os.path.join(baseprefix, "lib64"))
    rpaths.append(os.path.join(baseprefix, "usr/lib64"))

    for rpath in rpaths:
        # If rpath is already dynamic copy it to new_rpath and continue
        if rpath.find("$ORIGIN") != -1:
            new_rpath = rpath
        else:
            rpath =  os.path.normpath(rpath)
            new_rpath = "$ORIGIN/" + os.path.relpath(rpath, os.path.dirname(fpath.replace(rootdir, "/")))
        if not new_rpath in new_rpaths:
            new_rpaths.append(new_rpath)

    if break_hardlinks:
        bb.utils.break_hardlinks(fpath)

    args = ":".join(new_rpaths)
    try:
        subprocess.check_output(["patchelf", "--set-rpath", args, fpath],
        stderr=subprocess.PIPE, universal_newlines=True)
    except subprocess.CalledProcessError as e:
        bb.fatal("chrpath command failed with exit code %d:\n%s\n%s" % (e.returncode, e.stdout, e.stderr))


def get_rpms(d):
    rpms = []
    for uri in d.getVar('RPM_URI').split()[::2]:
        rpms.append(os.path.basename(bb.fetch.decodeurl(uri)[2]))
    return rpms

python () {
    import rhutils

    rpm_uris = d.getVar('RPM_URI')
    rpm_uris_and_sha256 = rpm_uris.split ()
    if len(rpm_uris_and_sha256) == 0:
       bb.warn("No rpms in RPM_URI")
       return

    rpms = []
    src_uris = list()
    for uri, sha256 in zip(rpm_uris_and_sha256[::2], rpm_uris_and_sha256[1::2]):
        bn = os.path.basename(bb.fetch.decodeurl(uri)[2])
        rpms.append(bn)
        # For name, replace some forbidden chars
        name = bn.replace("~", "-")
        src_uris.append("%s;unpack=0;name=%s" % (uri, name))
        if sha256 != "x":
            d.setVarFlag("SRC_URI", name + ".sha256sum", sha256)

    src_uris.append(d.getVar('SRC_URI'))
    d.setVar('SRC_URI', ' '.join(src_uris))

    pn = d.getVar('PN')
    if pn.endswith("-native"):
       name_suffix = "-native"
    elif pn.endswith("-cross"):
       name_suffix = "-cross"
    elif pn.find("-cross-") >= 0:
       name_suffix = pn[pn.index("-cross-"):]
    else:
       name_suffix = ""

    rpms = get_rpms(d);
    (name, ver, rel, epoch, arch) = rhutils.split_rpm_filename(rpms[0])

    packages = [ name ]
    for rpm in rpms[1:]:
        (name2, ver2, rel2, epoch2, arch2) = rhutils.split_rpm_filename(rpm)
        # TODO: Sometimes subpackages (like ncurses-term) is noarch, we just ignore that for now
        if (ver, rel, epoch, arch) != (ver2, rel2, epoch2, arch2) and arch2 != "noarch":
            bb.debug(2, "All rpms don't have same version, release and arch")
        packages.append(name2 )

    d.setVar('PACKAGES', ' '.join(map(lambda pkg: pkg + name_suffix, packages)))
    d.setVar('PR', rel)
    d.setVar('PV', ver)
    d.setVar('PACKAGE_ARCH', arch)
    d.setVar('DEFAULT_PREFERENCE', "2")

    def map_dependencies(deps, pn):
        deps = bb.utils.explode_deps(deps)
        newdeps = []
        for dep in deps:
            if dep == pn:
                continue
            elif "-cross-" in dep:
                newdeps.append(dep.replace("-cross", "-native"))
            elif not dep.endswith("-native"):
                newdeps.append(dep + "-native")
            else:
                newdeps.append(dep)
        return " ".join(newdeps)

    for pkg in packages:
        suffixed = pkg + name_suffix
        rprov = ' %s (=%s)' % (suffixed, ver)
        if name_suffix:
           old = d.getVar('RPROVIDES_%s' % (pkg))
           if old:
              old_mapped = map_dependencies (old, pn)
              rprov = rprov + " " + old_mapped
        d.appendVar('RPROVIDES_%s' % (suffixed), rprov)

    for pkg in packages:
        suffixed = pkg + name_suffix
        d.setVar('ALLOW_EMPTY_%s' % (suffixed), '1')
}

rpmbased_post_rpm_install() {
  # For some reason build-id files from dependent packages seem to get created, so just remove them all from the sysroots
  rm -rf ${D}${base_prefix}/usr/lib/.build-id
  sysroot-relativelinks.py ${D}${base_prefix}

  # Try to patch up pkg-config files as best we can
  # If this is not good enough, use a patch.
  for pcdir in ${D}${datadir}/pkgconfig ${D}${libdir}/pkgconfig ; do
    [ -d "$pcdir" ] || continue
    find $pcdir -type f -name "*.pc" -exec sed -i -e "s:^\([[:alnum:]_]*=\)\(/usr\|/etc\):\1${base_prefix}\2:"  {} \;
  done

 # Patch platform-python shebangs to env python3 so it works in the sysroot
 if [ -d ${D}${bindir} ] ; then
    pyfiles=$(find ${D}${bindir} . -type f -exec awk '  /^#!.*\/usr\/libexec\/platform-python/{print FILENAME} {nextfile}' {} +)
    for pyfile in $pyfiles; do
      sed -i -e "s:#!.*/usr/libexec/platform-python:#! /usr/bin/env python3:" $pyfile
    done
 fi
}

def try_unlink(path):
    try:
        os.unlink(path)
    except:
        pass

def setup_merged_usr(path):
    bb.utils.mkdirhier(os.path.join(path, "usr/bin"))
    bb.utils.mkdirhier(os.path.join(path, "usr/sbin"))
    bb.utils.mkdirhier(os.path.join(path, "usr/lib64"))
    bb.utils.mkdirhier(os.path.join(path, "usr/lib"))
    os.symlink("usr/bin", os.path.join(path, "bin"))
    os.symlink("usr/sbin", os.path.join(path, "sbin"))
    os.symlink("usr/lib64", os.path.join(path, "lib64"))
    os.symlink("usr/lib", os.path.join(path, "lib"))

# Unpacks rpm to directory and cleans up target dir permissions
def extract_rpm(d, rpm_path, extract_path):
    import subprocess, shutil, rhutils, tempfile

    bb.note("Extracting %s to %s" % (rpm_path, extract_path))

    tmpdir = d.getVar('TMPDIR')
    cpio_file = rhutils.rpm2cpio(rpm_path)

    with tempfile.NamedTemporaryFile(prefix=os.path.basename(rpm_path) + ".cpio-", mode="wb",dir=tmpdir) as cpio_tmpfile:
        shutil.copyfileobj(cpio_file, cpio_tmpfile)
        cpio_tmpfile.flush()
        cpio_file.close()

        cmd = 'cat %s | cpio -id --no-preserve-owner -D %s' % (cpio_tmpfile.name, extract_path)
        subprocess.run(cmd, check=True, shell=True)

    # Ensure that sstate staging can read all the files and remove them when cleaning
    cmd = 'chmod -R u+rwX %s' % (extract_path)
    subprocess.run(cmd, check=True, shell=True)

# Relative symlink from /usr/lib64 into /lib64/ break when used as sysroot coomponents, because the
# /lib64 symlink may not be there in the native sysroot, so we rewrite them back into /usr/lib64
def normalize_symlinks(extract_path):
    usr_lib64 = os.path.join(extract_path, "usr/lib64")
    if not os.path.isdir(usr_lib64):
        return
    for f in os.listdir(usr_lib64):
        f_path = os.path.join(usr_lib64, f)
        if os.path.islink(f_path):
          link_value = os.readlink(f_path)
          if link_value.startswith("../../lib64/"):
              fixed_link = link_value[len("../../lib64/"):]
              os.remove(f_path)
              os.symlink(fixed_link, f_path)


python rpmbased_do_install() {
    import subprocess, os, oe.patch

    pn = d.getVar('PN')
    workdir = d.getVar('WORKDIR')
    dest = d.getVar('D')
    base_prefix = d.getVar('base_prefix')
    if base_prefix:
       extract_path = os.path.join(dest, base_prefix[1:])
    else:
       extract_path = dest

    bb.note("Creating installation base dir %s" % (extract_path))
    bb.utils.mkdirhier(extract_path)

    # We need to manually set up the merged-usr symlinks, because
    # we don't have the filesystem rpm installed in the native sysroot
    #
    # Unfortunately this breaks for the filesystem package itself.
    # as the existence of e.g. usr/lib means it will apply the
    # dir permissions from the cpio (not writable) before extracting
    # children of it which gets a permission error.
    if not pn.startswith("filesystem"):
        setup_merged_usr(extract_path)

    for rpm in get_rpms(d):
        rpm_path = os.path.join(workdir, rpm)
        extract_rpm(d, rpm_path, extract_path)

    normalize_symlinks(extract_path)

    for patch in oe.patch.src_patches(d, False, True):
        _, _, local, _, _, parm = bb.fetch.decodeurl(patch)
        bb.note("Applying patch '%s' (%s)" % (parm['patchname'], oe.path.format_display(local, d)))
        cmd = 'cat %s | sed s:@@FIXMESTAGINGDIR@@:%s:g | patch -p1 -d %s' % (local, base_prefix, extract_path)
        subprocess.run(cmd, check=True, shell=True)

    bb.build.exec_func("post_rpm_install", d)
}

python rpmbased_do_package() {
    import rhutils

    # Init cachedpath
    global cpath
    cpath = oe.cachedpath.CachedPath()

    packages = (d.getVar('PACKAGES') or "").split()
    if len(packages) < 1:
        bb.error("No packages to build, skipping do_package")

    workdir = d.getVar('WORKDIR')
    outdir = d.getVar('DEPLOY_DIR')
    pn = d.getVar('PN')
    dest = d.getVar('D')                 # workdir/images
    dvar = d.getVar('PKGD')              # workdir/package
    pkgdest = d.getVar('PKGDEST')        # workdir/packages-split
    pkgdatadir = d.getVar('PKGDESTWORK') # workdir/pkgdata
    shlibswork_dir = d.getVar('SHLIBSWORKDIR')

    # Extract to packages-split as well, for emit_pkgdata
    for rpm in get_rpms(d):
        (pkgname, ver, rel, epoch, arch) = rhutils.split_rpm_filename(rpm)
        split_dir = os.path.join(pkgdest, pkgname)
        bb.utils.mkdirhier(split_dir)

        rpm_path = os.path.join(workdir, rpm)
        extract_rpm(d, rpm_path, split_dir)

        normalize_symlinks(split_dir)

    # Build global list of files in each split package
    global pkgfiles
    pkgfiles = {}
    for pkg in packages:
        pkgfiles[pkg] = []
        for walkroot, dirs, files in cpath.walk(os.path.join(pkgdest, pkg)):
            for file in files:
                pkgfiles[pkg].append(walkroot + os.sep + file)

    # Generate pkgdata

    bb.build.exec_func("package_prepare_pkgdata", d)
    bb.build.exec_func("package_do_filedeps", d)
    bb.build.exec_func("package_do_pkgconfig", d)

    shlib_provider = oe.package.read_shlib_providers(d)   # map soname => (map libdir => (pkg, pkgver))
    libdir = d.getVar('libdir')
    libsearchpath = [libdir, d.getVar('base_libdir')]

    # Extract provides from prebuilt rpms
    # Do provides before requirements as there can be dependencies between subpackages
    for rpm in get_rpms(d):
        (pkg, pkgver, _, _, _) = rhutils.split_rpm_filename(rpm)
        rpm_path = os.path.join(workdir, rpm)

        shlibs_file = os.path.join(shlibswork_dir, pkg + ".list")
        if os.path.exists(shlibs_file):
            os.remove(shlibs_file)

        soname_provs = d.getVar('RPM_SONAME_PROV_' + pkg)
        if not soname_provs:
            continue

        # If it provides a soname, add a ldconfig post-inst
        bb.debug(1, 'adding ldconfig call to postinst for %s' % pkg)
        postinst = d.getVar('pkg_postinst_%s' % pkg)
        if not postinst:
            postinst = '#!/bin/sh\n'
        postinst += d.getVar('ldconfig_postinst_fragment')
        d.setVar('pkg_postinst_%s' % pkg, postinst)

        # Update shlib provides
        bb.debug(2, "%s: provides sonames: %s" % (pkg, soname_provs))
        with open(shlibs_file, 'w') as fd:
            for soname in sorted(soname_provs.split (" ")):
                if soname in shlib_provider and libdir in shlib_provider[soname]:
                    (old_pkg, old_pkgver) = shlib_provider[soname][libdir]
                    if old_pkg != pkg:
                        bb.warn('%s-%s was registered as shlib provider for %s, changing it to %s-%s because it was built later' % (old_pkg, old_pkgver, soname, pkg, pkgver))
                bb.debug(1, 'registering %s-%s as shlib provider for %s' % (pkg, pkgver, soname))
                fd.write(soname + ':' + libdir + ':' + pkgver + '\n')
                if soname not in shlib_provider:
                    shlib_provider[soname] = {}
                shlib_provider[soname][libdir] = (pkg, pkgver)

    # Extract requirements from prebuilt rpms
    for rpm in get_rpms(d):
        pkg = rhutils.split_rpm_filename(rpm)[0]
        rpm_path = os.path.join(workdir, rpm)

        soname_deps = d.getVar('RPM_SONAME_REQ_' + pkg)
        if not soname_deps:
            continue

        deps = list()

        for soname in soname_deps.split(" "):
            if soname in shlib_provider.keys():
              shlib_provider_map = shlib_provider[soname]
              matches = set()
              for p in libsearchpath:
                  if p in shlib_provider_map:
                      matches.add(p)
              if len(matches) > 1:
                  matchpkgs = ', '.join([shlib_provider_map[match][0] for match in matches])
                  bb.error("%s: Multiple shlib providers for %s: %s" % (pkg, soname, matchpkgs))
              elif len(matches) == 1:
                  (dep_pkg, ver_needed) = shlib_provider_map[matches.pop()]

                  bb.debug(2, '%s: Dependency %s requires package %s' % (pkg, soname, dep_pkg))

                  if dep_pkg == pkg:
                      continue

                  if ver_needed:
                      dep = "%s (>= %s)" % (dep_pkg, ver_needed)
                  else:
                      dep = dep_pkg
                  if not dep in deps:
                      deps.append(dep)
                  continue
            bb.note("Couldn't find shared library provider for %s" % (soname))

        # Update shlibdeps for package
        deps_file = os.path.join(pkgdest, pkg + ".shlibdeps")
        if os.path.exists(deps_file):
            os.remove(deps_file)
        if deps:
            with open(deps_file, 'w') as fd:
                for dep in sorted(deps):
                    fd.write(dep + '\n')

    bb.build.exec_func("read_shlibdeps", d)
    bb.build.exec_func("package_depchains", d)
    bb.build.exec_func("emit_pkgdata", d)
}

python do_package_write_rpm() {
    import shutil

    workdir = d.getVar('WORKDIR')
    pkgd = d.getVar('PKGD')
    pkgdest = d.getVar('PKGDEST')

    package_arch = (d.getVar('PACKAGE_ARCH') or "").replace("-", "_")
    d.setVar('PACKAGE_ARCH_EXTEND', package_arch)
    if package_arch == "all":
        package_arch = "noarch"

    pkgwritedir = d.expand('${PKGWRITEDIRRPM}/${PACKAGE_ARCH_EXTEND}')
    bb.utils.mkdirhier(pkgwritedir)

    for rpm in get_rpms(d):
        shutil.copy2(os.path.join(workdir, rpm), pkgwritedir)
}

do_package_qa[noexec] = "1"


EXPORT_FUNCTIONS do_install post_rpm_install do_package
