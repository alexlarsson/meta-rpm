SUMMARY = "GNU binary utilities"
DESCRIPTION = "The GNU Binutils are a collection of binary tools. \
The main ones are ld (GNU Linker), and as (GNU Assembler). This \
package also includes addition tools such as addr2line (Converts \
addresses into filenames and line numbers), ar (utility for creating, \
modifying and extracting archives), nm (list symbols in object \
files), objcopy (copy and translate object files), objdump (Display \
object information), and other tools and related libraries."
HOMEPAGE = "http://www.gnu.org/software/binutils/"
BUGTRACKER = "http://sourceware.org/bugzilla/"
SECTION = "devel"
LICENSE = "GPLv3"

LIC_FILES_CHKSUM="\
    file://COPYING;md5=59530bdf33659b29e73d4adb9f9f6552\
    file://COPYING.LIB;md5=9f604d8a4f8e74f4f5140845a21b6674\
    file://COPYING3;md5=d32239bcb673463ab874e80d47fae504\
    file://COPYING3.LIB;md5=6a6a8e020838b23406c81b19c1d46df6\
    file://gas/COPYING;md5=d32239bcb673463ab874e80d47fae504\
    file://include/COPYING;md5=59530bdf33659b29e73d4adb9f9f6552\
    file://include/COPYING3;md5=d32239bcb673463ab874e80d47fae504\
    file://libiberty/COPYING.LIB;md5=a916467b91076e631dd8edb7424769c7\
    file://bfd/COPYING;md5=d32239bcb673463ab874e80d47fae504\
    "

def cross_suffix(d):
    build_arch = d.getVar("BUILD_ARCH")
    target_arch = d.getVar("TARGET_ARCH")
    if build_arch != target_arch:
      return "-cross-%s" % target_arch
    else:
      return "-disabled-cross-%s" % target_arch

# This is based on binutils-2.30-73 from centos:
PV = "2.30"
PR = "73"

PN = "binutils${@cross_suffix(d)}"
BPN = "binutils"

# Ignore how TARGET_ARCH is computed.
TARGET_ARCH[vardepvalue] = "${TARGET_ARCH}"

inherit cross
PROVIDES = "virtual/${TARGET_PREFIX}binutils"

# Tweak these from cross.bbclass so we match the rpmbased binutils
libdir = "${exec_prefix}/lib/"
libexecdir = "${exec_prefix}/libexec/"


INHIBIT_DEFAULT_DEPS = "1"
INHIBIT_AUTOTOOLS_DEPS = "1"

SRC_URI = "\
     https://ftp.gnu.org/gnu/binutils/binutils-2.30.tar.xz \
     file://binutils-2.19.50.0.1-output-format.sed \
     file://standards.info.gz \
     \
     file://binutils-2.20.51.0.2-libtool-lib64.patch \
     file://binutils-2.25-version.patch \
     file://binutils-2.22.52.0.1-export-demangle.h.patch \
     file://binutils-2.22.52.0.4-no-config-h-check.patch \
     file://binutils-2.26-lto.patch \
     file://binutils-2.29-filename-in-error-messages.patch \
     file://binutils-2.25-set-long-long.patch \
     file://binutils-readelf-other-sym-info.patch \
     file://binutils-2.27-aarch64-ifunc.patch \
     file://binutils-revert-PowerPC-speculation-barriers.patch \
     file://binutils-skip-dwo-search-if-not-needed.patch \
     file://binutils-page-to-segment-assignment.patch \
     file://binutils-2.30-allow_R_AARCH64-symbols.patch \
     file://binutils-strip-unknown-relocs.patch \
     file://binutils-speed-up-objdump.patch \
     file://binutils-2.28-ignore-gold-duplicates.patch \
     file://binutils-ifunc-relocs-in-notes.patch \
     file://binutils-debug-section-marking.patch \
     file://binutils-gold-llvm-plugin.patch \
     file://binutils-gas-build-notes.patch \
     file://binutils-CVE-2018-7642.patch \
     file://binutils-CVE-2018-7643.patch \
     file://binutils-CVE-2018-7208.patch \
     file://binutils-CVE-2018-10372.patch \
     file://binutils-CVE-2018-10373.patch \
     file://binutils-CVE-2018-7570.patch \
     file://binutils-CVE-2018-6323.patch \
     file://binutils-CVE-2018-6759.patch \
     file://binutils-CVE-2018-7569.patch \
     file://binutils-CVE-2018-7568.patch \
     file://binutils-CVE-2018-10534.patch \
     file://binutils-CVE-2018-10535.patch \
     file://binutils-x86-local-relocs.patch \
     file://binutils-linkonce-notes.patch \
     file://binutils-CVE-2018-8945.patch \
     file://binutils-x86-local-version.patch \
     file://binutils-fix-testsuite-failures.patch \
     file://binutils-PowerPC-IEEE-long-double-warnings.patch \
     file://binutils-missing-notes.patch \
     file://binutils-gold-ignore-discarded-note-relocs.patch \
     file://binutils-merge-attribute-sections.patch \
     file://binutils-remove-empty-ISA-properties.patch \
     file://binutils-note-merge-improvements.patch \
     file://binutils-gold-note-segment.patch \
     file://binutils-disable-readelf-gap-reports.patch \
     file://binutils-x86_64-disable-PLT-elision.patch \
     file://binutils-do-not-link-with-static-libstdc++.patch \
     file://binutils-attach-to-group.patch \
     file://binutils-CVE-2018-17358.patch \
     file://binutils-x86_64-ibt-enabled-tlsdesc.patch \
     file://binutils-gold-8-byte-note-segments.patch \
     file://binutils-nfp.patch \
     file://binutils-s390x-arch13.patch \
     file://binutils-s390x-partial-relro.patch \
     file://binutils-x86-IBT-and-missing-notes.patch \
     file://binutils-AArch64-gold.patch \
     file://binutils-multiple-relocs-for-same-section.patch \
     file://binutils-do-not-merge-differing-SHF_EXCLUDE-groups.patch \
     file://binutils-aarch64-STO_AARCH64_VARIANT_PCS.patch \
     file://binutils-coverity-fixes.patch \
     file://binutils-improved-note-merging.patch \
     file://binutils-CVE-2019-14444.patch \
     file://binutils-CVE-2019-1010204.patch \
     file://binutils-x86_JCC_Erratum.patch \
     file://binutils-CVE-2019-17451.patch \
     file://binutils-CVE-2019-17450.patch \
     file://binutils-copy-multiple-relocs.patch \
     \
     file://binutils-gcc10.patch \
"
SRC_URI[sha256sum] = "6e46b8aeae2f727a36f0bd9505e405768a72218f1796f0d09757d45209871ae6"


DEPENDS = "flex-native bison-native zlib-native gnu-config-native autoconf-native libgcc-native"

inherit autotools gettext multilib_header texinfo relative_symlinks

FILES_${PN} = " \
	${bindir}/${TARGET_PREFIX}* \
	${libdir}/lib*.so.* \
	${libdir}/lib*-${PV}*.so \
	${prefix}/${TARGET_SYS}/bin/* \
        ${bindir}/embedspu"

RPROVIDES_${PN} += "${PN}-symlinks"

FILES_${PN}-devel = " \
	${includedir} \
	${libdir}/*.la \
	${libdir}/libbfd.so \
	${libdir}/libctf.so \
	${libdir}/libctf-nobfd.so \
	${libdir}/libopcodes.so"

USE_ALTERNATIVES_FOR = " \
	addr2line \
	ld \
	ld.bfd \
	ld.gold \
"

python do_package_prepend() {
    make_alts = d.getVar("USE_ALTERNATIVES_FOR") or ""
    prefix = d.getVar("TARGET_PREFIX")
    bindir = d.getVar("bindir")
    for alt in make_alts.split():
        d.setVarFlag('ALTERNATIVE_TARGET', alt, bindir + "/" + prefix + alt)
        d.setVarFlag('ALTERNATIVE_LINK_NAME', alt, bindir + "/" + alt)
}

B = "${S}/build.${HOST_SYS}.${TARGET_SYS}"

EXTRA_OECONF = "--disable-werror \
                --enable-deterministic-archives=no \
                --enable-plugins \
                --enable-lto \
                --enable-compressed-debug-sections=none \
                --enable-targets=x86_64-pep \
                --enable-generate-build-notes=no \
                --enable-shared \
                --enable-threads=yes \
                --enable-relro=yes \
                --disable-gdb \
                --disable-gdbserver \
                --disable-libdecnumber \
                --disable-readline \
                --disable-sim \
                --enable-gold=default --enable-ld \
                --enable-64-bit-bfd \
                --with-libs=yes \
                "

# This is necessary due to a bug in the binutils Makefiles
EXTRA_OEMAKE = "MAKEINFO=true all"

export AR = "${HOST_PREFIX}ar"
export AS = "${HOST_PREFIX}as"
export LD = "${HOST_PREFIX}ld"
export NM = "${HOST_PREFIX}nm"
export RANLIB = "${HOST_PREFIX}ranlib"
export OBJCOPY = "${HOST_PREFIX}objcopy"
export OBJDUMP = "${HOST_PREFIX}objdump"

export AR_FOR_TARGET = "${TARGET_PREFIX}ar"
export AS_FOR_TARGET = "${TARGET_PREFIX}as"
export LD_FOR_TARGET = "${TARGET_PREFIX}ld"
export NM_FOR_TARGET = "${TARGET_PREFIX}nm"
export RANLIB_FOR_TARGET = "${TARGET_PREFIX}ranlib"

export CC_FOR_HOST = "${CCACHE}${HOST_PREFIX}gcc ${HOST_CC_ARCH}"
export CXX_FOR_HOST = "${CCACHE}${HOST_PREFIX}gcc ${HOST_CC_ARCH}"

# autotools.bbclass sets the _FOR_BUILD variables, but for some reason we need
# to unset LD_LIBRARY_PATH.
export CC_FOR_BUILD = "LD_LIBRARY_PATH= ${BUILD_CC}"

do_patch_append() {
    bb.build.exec_func('do_prepare', d)
}

# From centos binutils.spec %prep
do_prepare() {
	cd ${S}

	# On ppc64 and aarch64, we might use 64KiB pages
	sed -i -e '/#define.*ELF_COMMONPAGESIZE/s/0x1000$/0x10000/' bfd/elf*ppc.c
	sed -i -e '/#define.*ELF_COMMONPAGESIZE/s/0x1000$/0x10000/' bfd/elf*aarch64.c
	sed -i -e '/common_pagesize/s/4 /64 /' gold/powerpc.cc
	sed -i -e '/pagesize/s/0x1000,/0x10000,/' gold/aarch64.cc

	# LTP sucks
	perl -pi -e 's/i\[3-7\]86/i[34567]86/g' */conf*
	sed -i -e 's/%''{release}/${PR}/g' bfd/Makefile{.am,.in}
	sed -i -e '/^libopcodes_la_\(DEPENDENCIES\|LIBADD\)/s,$, ../bfd/libbfd.la,' opcodes/Makefile.{am,in}

	# Build libbfd.so and libopcodes.so with -Bsymbolic-functions if possible.
	if gcc %{optflags} -v --help 2>&1 | grep -q -- -Bsymbolic-functions; then
	sed -i -e 's/^libbfd_la_LDFLAGS = /&-Wl,-Bsymbolic-functions /' bfd/Makefile.{am,in}
	sed -i -e 's/^libopcodes_la_LDFLAGS = /&-Wl,-Bsymbolic-functions /' opcodes/Makefile.{am,in}
	fi

	# $PACKAGE is used for the gettext catalog name.
	sed -i -e 's/^ PACKAGE=/ PACKAGE=%{?cross}/' */configure
	# Undo the name change to run the testsuite.
	for tool in binutils gas ld
	do
	  sed -i -e "2aDEJATOOL = $tool" $tool/Makefile.am
	  sed -i -e "s/^DEJATOOL = .*/DEJATOOL = $tool/" $tool/Makefile.in
	done
	touch */configure
	chmod +x gold/testsuite/gnu_property_test.sh
}

do_configure () {
	oe_runconf
#
# must prime config.cache to ensure the build of libiberty
#
	mkdir -p ${B}/build-${BUILD_SYS}
	for i in ${CONFIG_SITE}; do
		cat $i >> ${B}/build-${BUILD_SYS}/config.cache || true
	done
}

inherit update-alternatives

ALTERNATIVE_PRIORITY = "100"
ALTERNATIVE_${PN}_class-target = "${USE_ALTERNATIVES_FOR}"

# Specify lib-path else we use a load of search dirs which we don't use
# and mean the linker scripts have to be relocated.
EXTRA_OECONF += "--with-sysroot=${STAGING_DIR_TARGET} \
                --enable-poison-system-directories \
                "
do_install () {
	oe_runmake 'DESTDIR=${D}' install

	# We don't really need these, so we'll remove them...
	rm -rf ${D}${STAGING_DIR_NATIVE}${prefix_native}/${TARGET_SYS}
	rm -rf ${D}${STAGING_DIR_NATIVE}${prefix_native}/lib/ldscripts

        # Move these to match rpmbased layout
        mv ${D}${STAGING_DIR_NATIVE}${prefix_native}/${BUILD_SYS}/${TARGET_SYS}/lib ${D}${STAGING_DIR_NATIVE}${prefix_native}/lib64
        mv ${D}${STAGING_DIR_NATIVE}/${prefix_native}/${BUILD_SYS}/${TARGET_SYS}/include ${D}${STAGING_DIR_NATIVE}${prefix_native}/include
}
