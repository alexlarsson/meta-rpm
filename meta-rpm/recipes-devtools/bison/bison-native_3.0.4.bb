include  bison_native.inc


SRC_URI = "${GNU_MIRROR}/bison/bison-${PV}.tar.xz \
           file://bison-3.0.4-c++-test-failure.patch \
           file://bison-3.0.4-gnulib-fseterr.patch \
           "
SRC_URI[sha256sum] = "a72428c7917bdf9fa93cb8181c971b6e22834125848cf1d03ce10b1bb0716fe1"
