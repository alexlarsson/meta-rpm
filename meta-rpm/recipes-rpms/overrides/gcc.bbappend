# This is used for gcc-native, the cross linker is in gcc-cross-host.bb

rpmbased_post_rpm_install_append() {
  # We only want the binaries, otherwise it conflicts with -cross
  rm -rf ${D}${libdir} \
         ${D}${datadir}
}
