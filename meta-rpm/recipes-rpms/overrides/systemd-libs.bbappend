post_rpm_install_append() {
  # This conflicts with the file from systemd, so remove
  rm ${D}${datadir}/licenses/systemd/LICENSE.LGPL2.1
}
