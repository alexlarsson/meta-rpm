# For whatever reason, libsolv-devel is not in the repos, so this is a rebuild of the srpm

SUMMARY = "generated recipe based on libsolv srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH_x86-64 = "x86_64"
PACKAGE_ARCH_aarch64 = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bzip2 libxml2 openssl rpm xz zlib"
RPM_SONAME_PROV_libsolv = "libsolv.so.1 libsolvext.so.1"
RPM_SONAME_REQ_libsolv = "libbz2.so.1 libc.so.6 libcrypto.so.1.1 liblzma.so.5 librpm.so.8 librpmio.so.8 libxml2.so.2 libz.so.1"
RDEPENDS_libsolv = "bzip2-libs glibc libxml2 openssl-libs rpm-libs xz-libs zlib"

RPM_URI_x86-64 = "file://libsolv-0.7.7-1.el8.x86_64.rpm x \
           file://libsolv-devel-0.7.7-1.el8.x86_64.rpm x \
           file://libsolv-tools-0.7.7-1.el8.x86_64.rpm x \
           file://python3-solv-0.7.7-1.el8.x86_64.rpm x \
           file://ruby-solv-0.7.7-1.el8.x86_64.rpm x \
           file://perl-solv-0.7.7-1.el8.x86_64.rpm x \
           file://libsolv-demo-0.7.7-1.el8.x86_64.rpm x \
          "
RPM_URI_aarch64 = "file://libsolv-0.7.7-1.el8.aarch64.rpm x \
           file://libsolv-devel-0.7.7-1.el8.aarch64.rpm x \
           file://libsolv-tools-0.7.7-1.el8.aarch64.rpm x \
           file://python3-solv-0.7.7-1.el8.aarch64.rpm x \
           file://ruby-solv-0.7.7-1.el8.aarch64.rpm x \
           file://perl-solv-0.7.7-1.el8.aarch64.rpm x \
           file://libsolv-demo-0.7.7-1.el8.aarch64.rpm x \
          "
