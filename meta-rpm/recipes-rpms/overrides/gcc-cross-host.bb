inherit cross
require ../generated/gcc.bb

EXTRADEPENDS = ""
NATIVEDEPS = "mpfr-native gmp-native libmpc-native zlib-native flex-native"

DEPENDS = "virtual/${TARGET_PREFIX}binutils ${EXTRADEPENDS} ${NATIVEDEPS}"
PROVIDES = "virtual/${TARGET_PREFIX}gcc virtual/${TARGET_PREFIX}g++"

BINV = "8"

PN = "gcc${@cross_suffix(d)}"
# Ignore how TARGET_ARCH is computed.
TARGET_ARCH[vardepvalue] = "${TARGET_ARCH}"

rpmbased_post_rpm_install_append() {
  # Collect all the dirs with binaries into one single prefix
  # dir so the automatic relocation code in gcc works
  mkdir -p ${D}/rhgcc
  mv ${D}/${prefix}/bin ${D}/rhgcc/
  mv ${D}/${prefix}/libexec ${D}/rhgcc/
  mv ${D}/${prefix}/lib ${D}/rhgcc/

  # This is a huge hack to have g++ find the /usr/include/c++/* files in
  # the target sysroot. I don't understand why it is needed, doesn't seem
  # to require this hack for poky.
  ln -s ../../../../../recipe-sysroot/usr/include/ ${D}/rhgcc/include

  # Then move that prefix into bindir (...usr/bin/x86_64-redhat-linux/)
  # so it will be staged in the native sysroot
  mkdir -p ${D}${bindir}
  mv ${D}/rhgcc ${D}${bindir}/

  # Then create symlinks to the binaries in bindir
  for tool in cpp c++ g++ gcc gcc-ar gcc-ranlib gcc-nm gcov gcov-dump gcov-tool; do
    ln -s rhgcc/bin/${tool} ${D}${bindir}/${TARGET_ARCH}-redhat-linux-${tool}
  done

  # Insert symlinks into libexec so when tools without a prefix are searched for, the correct ones are
  # found. These need to be relative paths so they work in different locations.
  link_dest=${D}${bindir}/rhgcc/libexec/gcc/${TARGET_SYS}/${BINV}/
  for t in ar as ld ld.bfd ld.gold nm objcopy objdump ranlib strip gcc cpp $fortsymlinks; do
      ln -sf ../../../../../${TARGET_PREFIX}$t ${link_dest}$t
      ln -sf ../../../../../${TARGET_PREFIX}$t ${link_dest}${TARGET_PREFIX}$t
  done

  find ${D}${bindir}/rhgcc/lib/gcc/${TARGET_SYS} -name "libgcc_s.so" -exec sed -i 's@/lib.*/libgcc_s.so.1@libgcc_s.so.1@g' {} \;
}
